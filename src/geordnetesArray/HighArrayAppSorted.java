package geordnetesArray;

// HighArray.java
////////////////////////////////////////////////////////////////
class HighArraySorted {
	private int[] a; // Array-Referenz
	private int nElems = 0; // Anzahl Elemente
	long max = 0;

	// -----------------------------------------------------------
	public HighArraySorted(int max) // Konstruktor
	{
		a = new int[max]; // Erzeuge Array
		nElems = 0; // Anzahl Elemente = 0
//		for (int i = 0; i < a.length; i++) {
//			a[i]=null;
//		}
	}

	// -----------------------------------------------------------
	public long find(long searchKey) { // Finde Element

		int indexBottom = 0;
		int indexTop = nElems-1;
		int middle = (nElems-1) / 2;

		while (searchKey != a[middle] && middle <= indexTop) {
			if (searchKey < a[middle]) {
				indexTop = middle-1;
				middle = (indexBottom +(indexTop - indexBottom)/2);
			} else {
				indexBottom = middle+1;
				middle = (indexBottom +(indexTop - indexBottom)/2);
			}

		}
		if (searchKey == a[middle]) {
			System.out.println("Element at pso.: " + middle);
			return middle;
		} else {
			System.out.println("Element not found");
			return -1;
		}

//		int j;
//		for (j = 0; j < nElems; j++) // F�r jedes Element,
//			if (a[j] == searchKey) // Gefunden?
//				break; // Abbrechen
//		if (j == nElems) // Ende erreicht?
//			return false; // Ja, kann es nicht finden
//		else
//			return true; // Nein, gefunden
	} // end find()
		// -----------------------------------------------------------

	public void insert(int value) // Element in Array einf�gen
	{
		for (int i = 0; i < a.length; i++) {
//			if (value == 0) {
//				break;
//			} else if (a[i] == 0) {
//				a[i] = value;
//				nElems++;
//				break;
//			}
			
			if (i>=nElems) {
				a[i]=value;
				nElems++;
				break;
			} 

			else if (value < a[i]) {
				int b = nElems + 1;
				while (b > i) {
					a[b] = a[b - 1];
					b--;
				}
				a[i] = value;
				nElems++;
				break;
			}
		}
// Original
//      a[nElems] = value;             // Einf�gen
//      nElems++;                      // Anzahl inkrementieren
	}

	// -----------------------------------------------------------
	public boolean delete(long value) {
		int j;
		for (j = 0; j < nElems; j++) // Suche es
			if (value == a[j])
				break;
		if (j == nElems) // Konnte es nicht finden
			return false;
		else // Gefunden
		{
			for (int k = j; k < nElems; k++) // Nachfolgende Elemente vorverschieben
				a[k] = a[k + 1];
			nElems--; // Anzahl dekrementieren
			return true;
		}
	} // end delete()
		// -----------------------------------------------------------

	public void display() // Array-Inhalt anzeigen
	{
		for (int j = 0; j < nElems; j++) // F�r jedes Element,
			System.out.print(a[j] + " "); // Zeige es
		System.out.println("");
	}
	// -----------------------------------------------------------

	public void getMax() // Array-Inhalt anzeigen
	{
		for (int j = 0; j < nElems; j++) { // F�r jedes Element,
			if (a[j] > max) {
				max = a[j];
			}

		}
		System.out.println("Max: " + max);
	}
	// -----------------------------------------------------------
} // end class HighArray
////////////////////////////////////////////////////////////////

class HighArrayAppSorted {
	public static void main(String[] args) {
		int maxSize = 25; // Array-Gr�sse
		HighArraySorted arr; // Array-Referenz
		arr = new HighArraySorted(maxSize); // Erzeuge Array mit gew�nschter Gr�sse

		arr.insert(77); // F�ge 10 Elemente ein
		arr.insert(99);
		arr.insert(44);
		arr.insert(55);
		arr.insert(2);
		arr.insert(88);
		arr.insert(-11);
		arr.insert(00); // ge�ndert, da bug.
		arr.insert(66);
		arr.insert(33);
		
		arr.display(); // Zeige den Array-Inhalt
		
		int searchKey = 100; // Suche Element 88
		arr.find(searchKey);
		
		arr.getMax();
		
		arr.delete(00); // L�sche drei Elemente (00, 55, 99)
		arr.delete(55);
		arr.delete(99);

		arr.display(); // Zeige den Array-Inhalt erneut
		
		arr.getMax();
	} // end main()
} // end class HighArrayApp

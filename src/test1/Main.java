package test1;

public class Main {

	public static void main(String[] args) {
		System.out.println("Eigener Sinus");
		System.out.println("-------------");
		System.out.println("Math.sin()");
		System.out.println(Math.sin(60));
		System.out.println("sinTaylor()");
		System.out.println(sinTaylor(60, 3));
		System.out.println();
		
		einmaleins();
	}
	
	private static void einmaleins() {
		System.out.println("1 x 1 Tabelle");
		System.out.println("-------------");
		System.out.println();
		
		for (int i = 1; i <= 10; i++) {
			System.out.print("\t"+i);

			for (int y= 1; y <= 10; y++) {
				System.out.print("\t"+i*y);
			}
			System.out.println();
		}
	}
	
	private static double sinTaylor(double x, int n) {
		double sum = 0;
		int temp = 0;
		for (int i = 0; i < n; i++) {
			temp = 0;
			for(int y=1;y<=2*i+1;y++) { 
				temp = temp + y ; 
			}

			sum += Math.pow(-1, i) * Math.pow(x, 2*i+1) / temp;
		}
		return sum;
	}

}

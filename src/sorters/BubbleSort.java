package sorters;

import myWatch.MyWatch;

public class BubbleSort {
	// 10 Elements 0.06ms
	// 1000 Elements 6ms
	// 100000 Elements 17s
	// 1000000 Elements 
	
	
	public static void main(String[] args) {
		int intArray[] = generateIntArray(1000000);
		//printArray(intArray);
		
		MyWatch.start();
		intArray = bubbleSort(intArray);
		MyWatch.stop();
		
		//printArray(intArray);
	}
	
	public static int[] bubbleSort (int[] ints) {
        for (int i = 0; i < ints.length - 1; i++) {
            for (int j = 0; j < ints.length - i - 1; j++) {
                if (ints[j] > ints[j + 1]) 
                { 
                    int cache = ints[j]; 
                    ints[j] = ints[j + 1]; 
                    ints[j + 1] = cache; 
                }
            }
            
            //printArray(ints);
        }
		
		return ints;
	}
	
	public static void printArray(int[] ints) {
        for (int i = 0; i < ints.length; ++i) {
            System.out.print(ints[i] + " "); 
        }
        System.out.println(); 
	}
	
	public static int[] generateIntArray(int size) {
		int[] ints = new int[size];
		for(int i=0; i < ints.length; i++)
	    {
			ints[i] = (int)(Math.random() * 100);
	    }
		return ints;
	}
}

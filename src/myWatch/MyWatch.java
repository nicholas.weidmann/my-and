package myWatch;

public class MyWatch {
	static long startTime;

	public static void main(String[] args) {
		start();
		for(int i=0; i< 5; i++){
            Object obj = new Object();
            elapsed();
        }
		stop();
	}

	public static void start() {
		startTime = System.nanoTime();
		System.out.println("Watch started at ns | " + startTime);
	}
	public static void elapsed() {
		double elapsedTime = System.nanoTime() - startTime;
		elapsedTime = elapsedTime/1000000;
		System.out.println("Watch elapsed in ms | " + elapsedTime);
	}
	public static void stop() {
		elapsed();
		System.out.println("Watch stopped at ns | " + System.nanoTime());
	}
}

package treppenstufen;

public class Main {
	static int counter;

	public static void main(String[] args) {
		// Treppen Werte eintragen
		// Start Stufe (0 lassen), Stufen, max Schrittgröse, Weg (leer lassen)
		steiger(0, 100, 2, "");
		System.out.println("Wege: " + counter);
	}
	
	public static void steiger(int stufe, int stufen, int maxSchrittGroesse, String weg) {
		if (stufe == stufen) {
			weg = weg.trim();
			System.out.print("Schritte: " + weg.split("\\s+").length + " Weg: ");
			System.out.println(weg);
			counter++;
			return;
		}
		
		for (int i = 1; i <= maxSchrittGroesse; i++) {
			if (i + stufe <= stufen) {
				steiger(stufe + i, stufen, maxSchrittGroesse, weg + " " + (stufe + i));
			}
		}
	}
}
